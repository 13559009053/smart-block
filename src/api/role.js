import http from '@/utils/request'

export default { 
  /**   * 查询角色列表   **/ 
  async getRoleListApi(params) { 
    return await http.get("/api/role/list", params); 
  },
  /**
   * 添加角色
   */
  async addRoleApi(params) { 
    return await http.post("/api/role/add", params);
  },
  /**
   *编辑角色 
   */
  async updateRoleApi(params) { 
    return await http.put("/api/role/update", params);
  },
  /**
   * 查询分配权限
   */
  async getAssignTreeApi(params){ 
    return await http.get("/api/role/getAssignPermissionTree",params);
  },
  /**
   * 分配权限
   */
  async assignSaveApi(params){ 
    return await http.post("/api/role/saveRoleAssign",params);
  },
  /** 
    * 检查角色是否被分配给用户
    */
   async checkRole(params){ 
    return await http.getRestApi("/api/role/check", params);
   },
   /** 
   * 删除部门 
   * @returns 
   */
  async deleteById(params) { 
    return await http.delete("/api/role/delete", params);
   }
}
