import http from '@/utils/request'

export default {
  /**
   * 查询所有传感器信息
   */
  async getSensorInformationListApi() {
    return await http.get("/api/sensorInformation/listAll");
  },
  /**
   * 查询未绑定传感器信息
   */
   async getSensorInformationNoBindApi() {
    return await http.get("/api/sensorInformation/getSensorInformationNoBind");
  },
  /**
   * 通过name查询传感器信息
   */
   async postSensorInformationByNameApi(params) {
    return await http.post("/api/sensorInformation/postSensorInformationByName", params);
  },
  /**
   * 通过name查询传感器个数
   */
   async postSensorInformationCountByNameApi(params) {
    return await http.post("/api/sensorInformation/postSensorInformationCountByName", params);
  },
  /**
   * 通过userName查询5条传感器信息
   */
   async postSensorInformationByUserNameApi(params) {
    return await http.post("/api/sensorInformation/postSensorInformationByUserName", params);
  },
  /**
   * 通过userName查询7条传感器信息
   */
   async postSensorInformationBindByUserNameSevenApi(params) {
    return await http.post("/api/sensorInformation/postSensorInformationBindByUserNameSeven", params);
  },
  /**
   * 通过userName查询5条传感器绑定信息
   */
   async postSensorInformationBindByUserNameApi(params) {
    return await http.post("/api/sensorInformation/postSensorInformationBindByUserName", params);
  },
  /**
   * 分页查询
   */
   async getSensorInformationListByNameApi(params) {
    return await http.get("/api/sensorInformation/list", params);
  },
  /**
   * 添加传感器信息
   */
   async addSensorInformationApi(params) {
    return await http.post("/api/sensorInformation/add", params);
  },
  /**
   * 修改传感器信息
   */
   async updateSensorInformationApi(params) {
    return await http.put("/api/sensorInformation/update", params);
  },
  /**
   * 删除传感器信息
   */
   async deleteById(params) {
    return await http.delete("/api/sensorInformation/delete", params);
   }
}
