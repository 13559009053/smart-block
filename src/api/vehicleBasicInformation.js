import http from '@/utils/request'

export default {
  /**
   * 通过carNumber查询
   */
   async getVehicleBasicInformationByCarNumberApi(params) {
    return await http.getRestApi("/api/vehicleBasicInformation/getVehicleBasicInformationByCarNumber", params);
  },
}
