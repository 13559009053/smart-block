import http from '@/utils/request'

export default {
  /**
   * 通过deviceId查询16条历史数据
   */
   async postSensorHistoricalDataByDeviceIdSixteenApi(params) {
    return await http.post("/api/sensorHistoricalData/postSensorHistoricalDataByDeviceIdSixteen", params);
  },
  /**
   * 通过deviceId查询历史数据
   */
   async postSensorHistoricalDataByDeviceIdApi(params) {
    return await http.post("/api/sensorHistoricalData/postSensorHistoricalDataByDeviceId", params);
  },
  /**
   * 查询总温度数
   */
   async totalRecordsApi() {
    return await http.get("/api/sensorHistoricalData/totalRecords");
  },
  /**
   * 查询异常温度设备
   */
   async unusualNumberApi() {
    return await http.get("/api/sensorHistoricalData/unusualNumber");
  },
  /**
   * 查询异常湿度设备
   */
   async unusualNumberHumidityApi() {
    return await http.get("/api/sensorHistoricalData/unusualNumberHumidity");
  },
  /**
   * 查询异常光敏设备
   */
   async unusualNumberIlluminationApi() {
    return await http.get("/api/sensorHistoricalData/unusualNumberIllumination");
  },
  /**
   * 分页查询历史数据
   */
   async getSensorHistoricalDataListByDeviceIdApi(params) {
    return await http.get("/api/sensorHistoricalData/list", params);
  },
  /**
   * 删除历史数据
   */
   async deleteById(params) {
    return await http.delete("/api/sensorHistoricalData/delete", params);
   }
}
