import http from '@/utils/request'

export default {
  /**
   * 通过sensorId查询实时数据列表
   */
   async getSensorDataByDeviceId(params){
    return await http.getRestApi("/api/sensorData/getSensorDataBySensorId",params);
  },
  /**
   * 分页查询实时数据
   */
   async getSensorDataListBySensorIdApi(params) {
    return await http.get("/api/sensorData/list", params);
  },
  /**
   * 添加实时数据
   */
   async addSensorDataApi(params) {
    return await http.post("/api/sensorData/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateSensorDataApi(params) {
    return await http.put("/api/sensorData/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/sensorData/delete", params);
   }
}
