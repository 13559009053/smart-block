import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getBusinessListByTypeApi(params) {
    return await http.get("/api/business/typeList", params);
  },
  /**
   * 分页查询实时数据
   */
   async getBusinessListByNameApi(params) {
    return await http.get("/api/business/list", params);
  },
  /**
   * 添加实时数据
   */
   async addBusinessApi(params) {
    return await http.post("/api/business/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateBusinessApi(params) {
    return await http.put("/api/business/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/business/delete", params);
   }
}
