import http from '@/utils/request'

export default {
  /**
   * 通过dutyId查询
   */
   async getSecurityOnDutyByIdApi(params) {
    return await http.getRestApi("/api/securityOnDuty/getSecurityOnDutyById", params);
  },
}
