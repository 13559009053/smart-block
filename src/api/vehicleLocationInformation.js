import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getVehicleLocationInformationListByCarNumberApi(params) {
    return await http.get("/api/vehicleLocationInformation/list", params);
  },
  /**
   * 添加实时数据
   */
   async addVehicleLocationInformationApi(params) {
    return await http.post("/api/vehicleLocationInformation/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateVehicleLocationInformationApi(params) {
    return await http.put("/api/vehicleLocationInformation/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/vehicleLocationInformation/delete", params);
   }
}
