import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getSecurityStaffListByNameApi(params) {
    return await http.get("/api/securityStaff/list", params);
  },
  /**
   * 添加实时数据
   */
   async addSecurityStaffApi(params) {
    return await http.post("/api/securityStaff/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateSecurityStaffApi(params) {
    return await http.put("/api/securityStaff/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/securityStaff/delete", params);
   }
}
