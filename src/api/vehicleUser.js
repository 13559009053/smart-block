import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getVehicleUserListBySexApi(params) {
    return await http.get("/api/vehicleUser/sexList", params);
  },
  /**
   * 分页查询实时数据
   */
   async getVehicleUserListByNameApi(params) {
    return await http.get("/api/vehicleUser/list", params);
  },
  /**
   * 添加实时数据
   */
   async addVehicleUserApi(params) {
    return await http.post("/api/vehicleUser/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateVehicleUserApi(params) {
    return await http.put("/api/vehicleUser/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/vehicleUser/delete", params);
   }
}
