import http from '@/utils/request'

export default {
  /**
   * 查询所有设备信息
   */
  async getDeviceInformationListApi() {
    return await http.get("/api/deviceInformation/listAll");
  },
  /**
   * 通过name分页查询设备信息
   */
   async getDeviceInformationListByNameApi(params) {
    return await http.get("/api/deviceInformation/list",params);
  },
  /**
   * 通过设备ID查询设备列表
   */
   async getDeviceInformationByDeviceId(params){
    return await http.getRestApi("/api/deviceInformation/getDeviceInformationByDeviceId",params);
  },
  /**
   * 通过设备name查询设备列表
   */
   async getDeviceInformationByDeviceName(params){
    return await http.getRestApi("/api/deviceInformation/getDeviceInformationByDeviceName",params);
  },
  /**
   * 添加设备
   */
   async addDeviceInformationApi(params) {
    return await http.post("/api/deviceInformation/add", params);
  },
  /**
   *编辑设备
   */
  async updateDeviceInformationApi(params) {
    return await http.put("/api/deviceInformation/update", params);
  },
  /**
   * 删除设备
   */
   async deleteById(params) {
    return await http.delete("/api/deviceInformation/delete", params);
   }
}
