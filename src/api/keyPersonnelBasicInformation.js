import http from '@/utils/request'

export default {
  /**
   * 分页查询重点人员基本信息
   */
   async getKeyPersonnelBasicInformationListByHazardDegreeApi(params) {
    return await http.get("/api/keyPersonnelBasicInformation/hazardDegreeList", params);
  },
  /**
   * 查询所有重点人员基本信息
   */
   async getKeyPersonnelBasicInformationListAll(params) {
    return await http.get("/api/keyPersonnelBasicInformation/listAll", params);
  },
  /**
   * 分页查询重点人员基本信息
   */
   async getKeyPersonnelBasicInformationListByNameApi(params) {
    return await http.get("/api/keyPersonnelBasicInformation/list", params);
  },
  /**
   * 添加重点人员基本信息
   */
   async addKeyPersonnelBasicInformationApi(params) {
    return await http.post("/api/keyPersonnelBasicInformation/add", params);
  },
  /**
   * 修改重点人员基本信息
   */
   async updateKeyPersonnelBasicInformationApi(params) {
    return await http.put("/api/keyPersonnelBasicInformation/update", params);
  },
  /**
   * 删除重点人员基本信息
   */
   async deleteById(params) {
    return await http.delete("/api/keyPersonnelBasicInformation/delete", params);
   }
}
