import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getBusinessTurnoverListByTurnoverApi(params) {
    return await http.get("/api/businessTurnover/list", params);
  },
  /**
   * 添加实时数据
   */
   async addBusinessTurnoverApi(params) {
    return await http.post("/api/businessTurnover/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateBusinessTurnoverApi(params) {
    return await http.put("/api/businessTurnover/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/businessTurnover/delete", params);
   }
}
