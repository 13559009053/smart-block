import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getBusinessWaterRentListByMoneyApi(params) {
    return await http.get("/api/businessWaterRent/list", params);
  },
  /**
   * 添加实时数据
   */
   async addBusinessWaterRentApi(params) {
    return await http.post("/api/businessWaterRent/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateBusinessWaterRentApi(params) {
    return await http.put("/api/businessWaterRent/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/businessWaterRent/delete", params);
   }
}
