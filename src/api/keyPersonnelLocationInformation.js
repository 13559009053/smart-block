import http from '@/utils/request'

export default {
  /**
   * 通过personnelId查询
   */
   async getKeyPersonnelLocationInformationByIdApi(params) {
    return await http.getRestApi("/api/keyPersonnelLocationInformation/getKeyPersonnelLocationInformationByPersonnelId", params);
  },
  /**
   * 通过所有重点人员位置信息
   */
   async listAllApi(params) {
    return await http.getRestApi("/api/keyPersonnelLocationInformation/listAll", params);
  },
}
