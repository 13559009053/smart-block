import http from '@/utils/request'

export default {
  /**
   * 分页查询实时数据
   */
   async getBusinessPowerRentListByMoneyApi(params) {
    return await http.get("/api/businessPowerRent/list", params);
  },
  /**
   * 添加实时数据
   */
   async addBusinessPowerRentApi(params) {
    return await http.post("/api/businessPowerRent/add", params);
  },
  /**
   * 修改实时数据
   */
   async updateBusinessPowerRentApi(params) {
    return await http.put("/api/businessPowerRent/update", params);
  },
  /**
   * 删除实时数据
   */
   async deleteById(params) {
    return await http.delete("/api/businessPowerRent/delete", params);
   }
}
